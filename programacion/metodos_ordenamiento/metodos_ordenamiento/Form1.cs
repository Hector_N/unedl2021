﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Algoritmos_de_ordenmaiento
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void label3_Click(object sender, EventArgs e)
        {

        }
        private void label2_Click(object sender, EventArgs e)
        {

        }
        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            string[] myArray = textBox1.Text.Split(new Char[] { ',' });
            int[] arr = new int[myArray.Length];

            for (int i = 0; i < myArray.Length; i++)
            {
                arr[i] = int.Parse(myArray[i]);
            }

            quicksort(arr,0,(arr.Length)-1);


        }

        private void quicksort(int[] vector, int primero, int ultimo)
        {
            int i, j, central;
            double pivote;
            central = (primero + ultimo) / 2;
            pivote = vector[central];
            i = primero;
            j = ultimo;
            do
            {
                while (vector[i] < pivote) i++;
                while (vector[j] > pivote) j--;
                if (i <= j)
                {
                    int temp;
                    temp = vector[i];
                    vector[i] = vector[j];
                    vector[j] = temp;
                    i++;
                    j--;
                }
            } while (i <= j);

            if (primero < j)
            {
                quicksort(vector, primero, j);
            }
            if (i < ultimo)
            {
                quicksort(vector, i, ultimo);
            }
            mostrar(vector, vector.Length);
        }

        private void mostrar(int[] vector, int h)
        {
            string salida="";
            for(int i = 0; i < vector.Length; i++)
            {
                salida = salida+ vector[i]+",";
            }
            label3.Text = salida;
        }


        private void button2_Click(object sender, EventArgs e)
        {
            string[] myArray = textBox1.Text.Split(new Char[] { ',' });
            int[] intArr = new int[myArray.Length];

            for (int i = 0; i < myArray.Length; i++)
            {
                intArr[i] = int.Parse(myArray[i]);
                Console.WriteLine(intArr[i]);
            }


            int count = 0;

            foreach (int caracter in intArr)
                label3.Text = caracter + " ";
            bool flag = true;
            for (int i = 1; (i <= (intArr.Length - 1)) && flag; i++)
            {
                flag = false;
                for (int j = 0; j < (intArr.Length - 1); j++)
                {
                    count = count + 1;
                    if (intArr[j + 1] > intArr[j])
                    {
                        int temp = intArr[j];
                        intArr[j] = intArr[j + 1];
                        intArr[j + 1] = temp;
                        flag = true;
                    }
                }
            }
            String Salida = "";
            foreach (int item in intArr)
            {
                Salida = Salida+ item+",";
                label3.Text = Salida;

            }
        }

        private void btnPruebas_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            string[] myArray = textBox1.Text.Split(new Char[] { ',' });
            int[] intArr = new int[myArray.Length];

            for (int i = 0; i < myArray.Length; i++)
            {
                intArr[i] = int.Parse(myArray[i]);
            }

            label3.Text = string.Join(",",MergeSort(intArr));
        }
        static int[] MergeSort(int[] array)
        {
            return MergeSort(array, 0, array.Length - 1);
        }
        static int[] MergeSort(int[] array, int lowIndex, int highIndex)
        {
            if (lowIndex < highIndex)
            {
                var middleIndex = (lowIndex + highIndex) / 2;
                MergeSort(array, lowIndex, middleIndex);
                MergeSort(array, middleIndex + 1, highIndex);
                Merge(array, lowIndex, middleIndex, highIndex);
            }

            return array;
        }
        static void Merge(int[] array, int lowIndex, int middleIndex, int highIndex)
        {
            var left = lowIndex;
            var right = middleIndex + 1;
            var tempArray = new int[highIndex - lowIndex + 1];
            var index = 0;

            while ((left <= middleIndex) && (right <= highIndex))
            {
                if (array[left] < array[right])
                {
                    tempArray[index] = array[left];
                    left++;
                }
                else
                {
                    tempArray[index] = array[right];
                    right++;
                }

                index++;
            }

            for (var i = left; i <= middleIndex; i++)
            {
                tempArray[index] = array[i];
                index++;
            }

            for (var i = right; i <= highIndex; i++)
            {
                tempArray[index] = array[i];
                index++;
            }

            for (var i = 0; i < tempArray.Length; i++)
            {
                array[lowIndex + i] = tempArray[i];
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string[] myArray = textBox1.Text.Split(new Char[] { ',' });
            int[] intArr = new int[myArray.Length];

            for (int i = 0; i < myArray.Length; i++)
            {
                intArr[i] = int.Parse(myArray[i]);
            }

            heapsort(intArr);
            PrintArray(intArr);
        }
        static void heapsort(int[] Array)
        {
            int n = Array.Length;
            int temp;
            for (int i = n / 2; i >= 0; i--)
            {
                heapify(Array, n - 1, i);
            }
            for (int i = n - 1; i >= 0; i--)
            {
                //swap last element of the max-heap with the first element
                temp = Array[i];
                Array[i] = Array[0];
                Array[0] = temp;

                //exclude the last element from the heap and rebuild the heap 
                heapify(Array, i - 1, 0);
            }
        }
        static void heapify(int[] Array, int n, int i)
        {
            int max = i;
            int left = 2 * i + 1;
            int right = 2 * i + 2;

            //if the left element is greater than root
            if (left <= n && Array[left] > Array[max])
            {
                max = left;
            }

            //if the right element is greater than root
            if (right <= n && Array[right] > Array[max])
            {
                max = right;
            }

            //if the max is not i
            if (max != i)
            {
                int temp = Array[i];
                Array[i] = Array[max];
                Array[max] = temp;
                //Recursively heapify the affected sub-tree
                heapify(Array, n, max);
            }
        }
        private void PrintArray(int[] Array)
        {
            var salida="";
            for (int i = 0; i < Array.Length; i++)
            {
                salida = salida + Array[i]+",";
            }
            label3.Text = salida;
        }
    }

}
    


