﻿using System;

namespace pl
{
    class Program
    {
        static unsafe void Main(string[] args)
        {
            int var = 20;
            int* p = &var;
            Console.WriteLine("data is {0}", var);
            Console.WriteLine("", (int)p);
            Console.ReadKey();
        }
    }
}
