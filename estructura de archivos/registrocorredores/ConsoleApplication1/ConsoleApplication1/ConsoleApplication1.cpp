// ConsoleApplication1.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//

#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include <cstring>



struct Corredor {
	int id_carrera;
	char nombrecarrera[20];
	char numcorredor[5];
	std::string categoria;
	
} corredor;

struct Personas {
	int id_persona;
	char nombre[20];
	char apellido[20];
	Corredor corr;
	int telefono;
	char email[50];
	int edad;
} persona[2];


int main()
{
	std::ofstream outfile;
	outfile.open("personas.txt");
	int opcion;
	std::cout << "Digite el numero de corredores que va a registrar: \n";
	std::cin >> opcion;
	
	for (int i = 0; i < opcion; i++) {
		fflush(stdin);

		std::cout << "Digite el id: ";
		std::cin >> persona[i].id_persona;

		//std::cin.ignore(numeric_limits<streamsize>::max(), '\n');
		std::cout << "Nombre: ";
		std::cin>>persona[i].nombre;


		std::cout << "Apellido: ";
		std::cin>>persona[i].apellido;

		std::cout << "Telefono: ";
		std::cin>>persona[i].telefono;
		//std::cin.ignore(numeric_limits<streamsize>::max(), '\n');

		std::cout << "Email: ";
		std::cin>>persona[i].email;

		std::cout << "Edad: ";
		std::cin >>persona[i].edad;

		std::cout << "Digite el id de la carrera: ";
		std::cin >> persona[i].corr.id_carrera;

		std::cout << "Nombre de la carrera: ";
		std::cin>>persona[i].corr.nombrecarrera;

		std::cout << "Numero de corredor: ";
		std::cin>>persona[i].corr.numcorredor;


		std::cout ;
	}

	std::cout << "\n ---impresion de datos--- \n";
	for (int i = 0; i < opcion; i++) {
		std::cout << "id de la persona: \n" << persona[i].id_persona ;
		outfile << persona[i].id_persona ;
		std::cout << "\nNombre: \n" << persona[i].nombre ;
		outfile << persona[i].nombre ;
		std::cout << "\nApellido: \n" << persona[i].apellido ;
		outfile << persona[i].apellido;
		std::cout << "\nTelefono: \n" << persona[i].telefono ;
		outfile << persona[i].telefono ;
		std::cout << "\nEmail: \n" << persona[i].email ;
		outfile << persona[i].email ;
		std::cout << "\nEdad: \n" << persona[i].edad ;
		outfile << persona[i].edad ;
		std::cout << "\nid de la carrera: \n" << persona[i].corr.id_carrera;
		outfile << persona[i].corr.id_carrera;
		std::cout << "\nNombre de la carrera: \n" << persona[i].corr.nombrecarrera;
		outfile << persona[i].corr.nombrecarrera;
		std::cout << "\nNumero de corredor: \n" << persona[i].corr.numcorredor;
		outfile << persona[i].corr.numcorredor;

		if (persona[i].edad<=18) {
			persona[i].corr.categoria="juvenil"; 
		}
		else if (persona[i].edad > 18|| persona[i].edad <= 50) {

			persona[i].corr.categoria="senior";
		}
		else if (persona[i].edad > 51) {

			persona[i].corr.categoria="veterano";
		}

		std::cout << "\nCategoria: \n" << persona[i].corr.categoria;
		outfile << persona[i].corr.categoria ;

		std::cout << "\n----------\n" ;
	}
	outfile.close();
}

// Ejecutar programa: Ctrl + F5 o menú Depurar > Iniciar sin depurar
// Depurar programa: F5 o menú Depurar > Iniciar depuración

// Sugerencias para primeros pasos: 1. Use la ventana del Explorador de soluciones para agregar y administrar archivos
//   2. Use la ventana de Team Explorer para conectar con el control de código fuente
//   3. Use la ventana de salida para ver la salida de compilación y otros mensajes
//   4. Use la ventana Lista de errores para ver los errores
//   5. Vaya a Proyecto > Agregar nuevo elemento para crear nuevos archivos de código, o a Proyecto > Agregar elemento existente para agregar archivos de código existentes al proyecto
//   6. En el futuro, para volver a abrir este proyecto, vaya a Archivo > Abrir > Proyecto y seleccione el archivo .sln
