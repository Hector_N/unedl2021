﻿using System;
 using System.IO;

  

    namespace uso_de_archivos_Cs
    {
        class Program
        {
            //FileStream fs ;
            static void Main(string[] args)
            {
                string op, nombreArchivo = "";
                Program program = new Program();

                do
                {
                    Console.Clear();
                    Console.Write("a) Crear Archivo\n" +
                        "b) Agregar registro\n" +
                        "c) Leer los registros del archivo\n" +
                        "d) Borrar contenido del arhivo\n" +
                        "e) Salir\n" +
                        "Seleccione una opción: ");
                    op = Console.ReadLine();

                    switch (op)
                    {
                        case "a":
                            nombreArchivo = program.crearArchivo();
                            break;
                        case "b":
                            program.agregarRegistro(nombreArchivo);
                            break;
                        case "c":
                            program.leerRegistro(nombreArchivo);
                            break;
                        case "d":
                            program.borrarContenido(nombreArchivo);
                            break;
                        case "e":
                            Console.WriteLine("Saliendo...");
                            break;
                        default:
                            Console.WriteLine("Error, opcion invalida");
                            break;
                    }
                } while (op != "e");

            }

            string crearArchivo()
            {
                Console.Clear();
                Console.WriteLine("\nCreando el archivo...");
                Console.Write("Ingrese el nombre del archivo: ");
                string nombreArchivo = Console.ReadLine();
                nombreArchivo = nombreArchivo + ".txt";
                using (FileStream fs = new FileStream(nombreArchivo, FileMode.OpenOrCreate))
                {
                    fs.Close();
                }
                return nombreArchivo;
            }

            void agregarRegistro(string nombreArchivo)
            {
                Console.Clear();
                Console.Write("Ingrese el nombre del archivo: ");
                string nombre = Console.ReadLine();
                nombre += ".txt";
                if (nombre.Equals(nombreArchivo))
                {
                    using (StreamWriter sw = File.AppendText(nombre))
                    {
                        Console.Write("Ingrese el texto: ");
                        string texto = Console.ReadLine();
                        sw.WriteLine(texto);
                    }
                }
                else
                    Console.WriteLine("Error, nombre incorrecto");
            }

            void leerRegistro(string nombreArchivo)
            {
                Console.Clear();
                Console.Write("Ingrese el nombre del archivo: ");
                string nombre = Console.ReadLine();
                nombre += ".txt";
                if (nombre.Equals(nombreArchivo))
                {
                    using (StreamReader sr = File.OpenText(nombre))
                    {
                        if (new FileInfo(nombre).Length != 0)
                        {
                            string texto = "";
                            while ((texto = sr.ReadLine()) != null)
                            {
                                Console.WriteLine(texto);
                            }
                        }
                        else
                            Console.WriteLine("El archivo no tiene ningún texto");
                    }
                }
                else
                    Console.WriteLine("Error, nombre incorrecto");
                Console.WriteLine("Presione cualquier tecla para continuar");
                Console.ReadKey();
            }

            void borrarContenido(string nombreArchivo)
            {
                Console.Clear();
                Console.Write("Ingrese el nombre del archivo: ");
                string nombre = Console.ReadLine();
                nombre += ".txt";
                if (nombre.Equals(nombreArchivo))
                {
                    using FileStream fs = new FileStream(nombre, FileMode.Truncate);
                    fs.Close();
                }
                else
                    Console.WriteLine("Error, nombre incorrecto");
            }

        }
    }

