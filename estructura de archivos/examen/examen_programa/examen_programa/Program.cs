﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimerExamenParcial
{
    class Program
    {

        // Un palíndromo es una palabra o frase que se lee igual 
        // de izquierda a derecha que de derecha a izquierda.
        static bool isPalindromo(string word)
        {
            bool resultado = true;
           
            string wordmin = word.ToLower();
            wordmin = wordmin.Replace(" ", String.Empty);
            wordmin = wordmin.Replace("á", "a");
            wordmin = wordmin.Replace("é", "e");
            wordmin = wordmin.Replace("í", "i");
            wordmin = wordmin.Replace("ó", "o");
            wordmin = wordmin.Replace("ú", "u");
            char[] arr = wordmin.ToCharArray();
            char[] wordarr = wordmin.ToCharArray();
            Array.Reverse(arr);
            
            Console.WriteLine(arr);
            Console.WriteLine(wordarr);

  
            for (int i=0;i<wordarr.Length;i++)
            {
                if(wordarr[i]==arr[i])
                {
                    
                }
                else
                {
                   
                    resultado = false;
                }
            }
            return resultado;


        }

        // Metodo principal
        static void Main(string[] args)
        {
            string[] sarray = { "Acaso hubo búhos acá", "A cavar a Caravaca",
                "Allí ves Sevilla", "Amad a la dama" };

            foreach (string word in sarray)
            {
                
               
                if (isPalindromo(word))
                {
                Console.WriteLine("La palabra {0} es un palíndromo", word);
                }
                else
                {
                    Console.WriteLine("La palabra {0} no es palíndromo", word);
                }
                    
            }
        }
    }
}
